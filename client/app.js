import Vue from "vue";

import VueRouter from "vue-router";
import Bootstrap from "bootstrap-vue";
import VueCurrency from "vue-currency-filter";
import Notifications from "vue-notification";
import Validate from "./plugins/Validate";
import Modules from "./plugins/Modules";
import Paginator from "./plugins/Paginator";
import Moment from "./plugins/Moment";
import Backup from "./plugins/Backup";

import router from "./router/router";
import store from "./store";

import Main from "./Main";

Vue.use(VueRouter);
Vue.use(Validate);
Vue.use(Bootstrap);
Vue.use(VueCurrency, { symbol: "$" });
Vue.use(Notifications);
Vue.use(Paginator);
Vue.use(Moment);
Vue.use(Backup);
Vue.use(Modules);

const MainVue = Vue.extend(Main);

new MainVue({
    router,
    store,
    async created() {
        try {
            await this.$store.dispatch("INIT");
        } catch (error) {
            this.$store.commit("SET_STATUS", { error, display: "ERROR" });
        }
    }
}).$mount("#app");
