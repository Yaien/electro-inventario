export default {
    data: () => ({
        roles: [
            { text: "Administrador", value: "admin" },
            { text: "Empleado", value: "empleado" }
        ]
    })
};
