export default {
    props: ["selected"],
    data() {
        return {
            producto: {},
            status: null,
            dispatch: "UPDATE_PRODUCTO"
        };
    },
    methods: {
        async submit() {
            try {
                await this.$store.dispatch(this.dispatch, this.payload());
                this.$emit("updated");
                this.clear();
                this.reset();
                this.$notify("<h5>Producto Actualizado!</h5>");
            } catch (e) {
                this.status = e.message;
            }
        },
        reset() {
            this.producto = {};
        },
        clear() {
            this.status = null;
        },
        payload() {
            return { producto: this.selected, update: this.producto };
        }
    },
    watch: {
        selected(producto) {
            this.producto = producto ? { ...producto } : null;
            this.clear();
        }
    }
};
