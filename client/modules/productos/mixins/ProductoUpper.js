export default {
    watch: {
        "producto.nombre": function(nombre) {
            if (this.producto && this.producto.nombre) {
                this.producto.nombre = nombre.toUpperCase();
            }
        }
    }
};
