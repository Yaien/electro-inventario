import { auth, guess, admin, firstUser } from "./guardias";
import Dash from "../pages/Dash";
import Home from "../pages/Home";
import Empleados from "../pages/Empleados";
import Facturas from "../pages/Facturas";
import Historial from "../pages/Historial";
import Productos from "../pages/Productos";
import Auth from "../pages/Auth";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Perfil from "../pages/Perfil";
import PasswordReset from "../pages/PasswordReset";
import Ventas from "../pages/Ventas";
import Pedidos from "../pages/Pedidos";
import Diario from "../pages/Diario";
import Backup from "../pages/Backup";

export default [
    {
        path: "/",
        component: Dash,
        beforeEnter: auth,
        children: [
            { path: "", component: Home },
            { path: "productos", component: Productos, beforeEnter: admin },
            { path: "facturas", component: Facturas },
            { path: "empleados", component: Empleados, beforeEnter: admin },
            { path: "backup", component: Backup, beforeEnter: admin },
            { path: "perfil", component: Perfil },
            {
                path: "historial",
                component: Historial,
                redirect: "historial/diario",
                children: [
                    { path: "ventas", component: Ventas },
                    { path: "pedidos", component: Pedidos },
                    { path: "diario", component: Diario }
                ]
            }
        ]
    },
    {
        path: "/auth",
        component: Auth,
        beforeEnter: guess,
        children: [
            { path: "login", component: Login },
            { path: "register", component: Register, beforeEnter: firstUser },
            { path: "reset", component: PasswordReset }
        ]
    }
];
