import store from "../store";

export function auth(to, from, next) {
    if (!store.state.auth.usuario) return next("/auth/login");
    next();
}

export function guess(to, from, next) {
    if (store.state.auth.usuario) return next("/");
    next();
}

export function admin(to, from, next) {
    let usuario = store.state.auth.usuario;
    if (usuario && usuario.rol == "admin") return next();
    next(false);
}

export function firstUser(to, from, next) {
    if (store.state.empleados.empleados.length) return next(false);
    next();
}
