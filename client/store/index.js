import Vue from "vue";
import Vuex from "vuex";
import fatal from "./modules/fatal";
import auth from "./modules/auth";
import productos from "./modules/productos";
import factura from "./modules/factura";
import empleados from "./modules/empleados";
import estadisticas from "./modules/estadisticas";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        fatal,
        auth,
        productos,
        factura,
        empleados,
        estadisticas
    },
    actions: {
        async INIT({ dispatch }) {
            await dispatch("FETCH_EMPLEADOS");
            await dispatch("AUTH_INIT");
        }
    }
});

export default store;
