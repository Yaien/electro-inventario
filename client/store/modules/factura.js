import db from "../services/dexie";

const facturas = db.table("facturas");
const productos = db.table("productos");

export default {
  state: {
    facturas: []
  },

  mutations: {
    SET_FACTURAS(state, facturas) {
      state.facturas = facturas;
    },

    ADD_FACTURA(state, factura) {
      state.facturas = [factura, ...state.facturas];
    },
    REMOVE_FACTURA(state, id) {
      state.facturas = state.facturas.filter(f => f.id !== id);
    }
  },

  actions: {
    async STORE_FACTURA(store, payload) {
      payload.productos.forEach(p => (p.existencia -= p.cantidad));
      let factura = { ...payload, id: new Date() };
      await facturas.add(factura);
      for (let prod of payload.productos) {
        await productos.update(prod.id, {
          existencia: prod.existencia
        });
      }
      store.commit("ADD_FACTURA", factura);
    },

    async FETCH_FACTURAS(store) {
      store.commit("SET_FACTURAS", await facturas.reverse().toArray());
    },

    async CANCELAR_FACTURA(store, factura) {
      for (let item of factura.productos) {
        let producto = await productos.get(item.id);
        let existencia = producto.existencia + item.cantidad;
        await productos.update(item.id, { existencia });
      }
      await facturas.delete(factura.id);
      store.commit("REMOVE_FACTURA", factura.id);
    }
  }
};
