import db from "../services/dexie";

const productos = db.table("productos");
const pedidos = db.table("pedidos");

export default {
    state: {
        productos: [],
        pedidos: []
    },
    mutations: {
        SET_PRODUCTOS(state, productos) {
            state.productos = productos;
        },
        ADD_PRODUCTO(state, producto) {
            state.productos = [...state.productos, producto];
        },
        UPDATE_PRODUCTO(state, producto) {
            state.productos = state.productos.map(
                p => (p.id == producto.id ? producto : p)
            );
        },
        ADD_PEDIDO(state, pedido) {
            state.pedidos = [...state.pedidos, pedido];
        },
        SET_PEDIDOS(state, pedidos) {
            state.pedidos = pedidos;
        }
    },
    actions: {
        async FETCH_PRODUCTOS(store) {
            store.commit("SET_PRODUCTOS", await productos.toArray());
        },
        async FETCH_PEDIDOS(store) {
            store.commit("SET_PEDIDOS", await pedidos.toArray());
        },
        async ADD_PRODUCTO(store, payload) {
            if (await productos.where({ nombre: payload.nombre }).count())
                throw new Error("Producto ya registrado");
            payload.id = await productos.add(payload);
            store.commit("ADD_PRODUCTO", payload);
        },
        async UPDATE_PRODUCTO(store, { producto, update }) {
            let id = producto.id;
            let query = productos
                .where("nombre")
                .equals(update.nombre)
                .and(prod => prod.id !== id);
            if (await query.count())
                throw new Error(
                    `Ya existe un producto con el nombre ${update.nombre}`
                );
            productos.update(id, update);
            store.commit("UPDATE_PRODUCTO", update);
        },
        async PEDIR_PRODUCTO(store, { producto, cantidad }) {
            let pedido = {
                id: new Date(),
                cantidad,
                producto: producto.nombre
            };
            await pedidos.add(pedido);
            await productos.update(producto.id, {
                existencia: producto.existencia + cantidad
            });
            store.commit("UPDATE_PRODUCTO", await productos.get(producto.id));
            store.commit("ADD_PEDIDO", pedido);
        },
        async FETCH_PEDIDOS(store) {
            store.commit("SET_PEDIDOS", await pedidos.toArray());
        }
    },
    getters: {
        searchProduct: state => search =>
            state.productos.filter(p =>
                p.nombre.match(new RegExp(search, "gi"))
            )
    }
};
