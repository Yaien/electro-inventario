import db from "../services/dexie";
import md5 from "md5";

const usuarios = db.table("usuarios");

export default {
    state: {
        empleados: []
    },
    mutations: {
        SET_EMPLEADOS(state, empleados) {
            state.empleados = empleados;
        },
        ADD_EMPLEADO(state, empleado) {
            state.empleados = [...state.empleados, empleado];
        },
        BORRAR_EMPLEADO(state, empleado) {
            state.empleados = state.empleados.filter(
                emp => emp.id != empleado.id
            );
        }
    },
    actions: {
        async FETCH_EMPLEADOS(store) {
            store.commit("SET_EMPLEADOS", await usuarios.toArray());
        },
        async REGISTRAR_EMPLEADO(store, payload) {
            if (await usuarios.where({ correo: payload.correo }).count())
                throw Error("Correo ya registrado");
            payload.contraseña = md5(payload.contraseña);
            let id = await usuarios.add(payload);
            store.commit("ADD_EMPLEADO", { ...payload, id });
        },
        async BORRAR_EMPLEADO(store, empleado) {
            await usuarios.delete(empleado.id);
            store.commit("BORRAR_EMPLEADO", empleado);
        }
    }
};
