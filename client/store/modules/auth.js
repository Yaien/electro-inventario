import db from "../services/dexie";
import session from "../services/session";
import { email } from "../services/functions";
import md5 from "md5";

const usuarios = db.table("usuarios");

export default {
    state: {
        usuario: null,
        ready: false
    },
    mutations: {
        LOGIN(state, usuario) {
            session.id = usuario.id;
            state.usuario = usuario;
        },
        LOGOUT(state) {
            session.clear();
            state.usuario = null;
        },
        READY(state) {
            state.ready = true;
        },
        UPDATE_USER(state, usuario) {
            state.usuario = usuario;
        }
    },
    actions: {
        async LOGIN(store, payload) {
            let usuario = await usuarios.get({ correo: payload.correo });
            if (!usuario) throw Error("Usuario no registrado");
            if (usuario.contraseña != md5(payload.contraseña))
                throw Error("Contraseña incorrecta");
            store.commit("LOGIN", usuario);
        },

        async REGISTER(store, payload) {
            if (await usuarios.where({ correo: payload.correo }).count())
                throw Error("Correo ya registrado");
            let usuario = { ...payload, rol: "admin" };
            usuario.contraseña = md5(payload.contraseña);
            usuario.id = await usuarios.add(usuario);
            store.commit("LOGIN", usuario);
            store.dispatch("FETCH_EMPLEADOS");
        },

        async AUTH_INIT(store) {
            if (!session.id) {
                store.commit("READY");
                return;
            }
            let usuario = await usuarios.get(session.id);
            if (usuario) store.commit("LOGIN", usuario);
            else store.commit("LOGOUT");
            store.commit("READY");
        },

        async UPDATE_PROFILE(store, payload) {
            let id = store.state.usuario.id;
            let query = usuarios
                .where("correo")
                .equals(payload.correo)
                .and(user => user.id !== id);

            if (await query.count()) throw Error("Correo ya registrado");
            await usuarios.update(id, payload);
            store.commit("UPDATE_USER", await usuarios.get(id));
        },

        async UPDATE_PASSWORD(store, { old_pass, new_pass }) {
            let { id, contraseña } = store.state.usuario;
            if (contraseña != md5(old_pass))
                throw Error("Contraseña incorrecta");
            let payload = { contraseña: md5(new_pass) };
            await usuarios.update(id, payload);
            store.commit("UPDATE_USER", await usuarios.get(id));
        },

        async SEND_PASSWORD_RESET(store, correo) {
            let usuario = await usuarios.get({ correo });
            if (usuario == null) throw Error("Usuario no registrado");
            let contraseña = email.sendPasswordReset(usuario);
            usuarios.update(usuario.id, {
                contraseña: md5(contraseña)
            });
        }
    }
};
