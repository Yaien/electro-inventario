export default {
    state: {
        error: null,
        display: null
    },
    mutations: {
        SET_STATUS(state, { error, display }) {
            console.error(error)
            state.error = error
            state.display = display
        },
        CLEAR_STATUS(state) {
            state.error = null
            state.display = null
        }
    }
}