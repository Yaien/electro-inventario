import { sumBy } from "lodash";
import moment from "moment";

export default {
  namespaced: true,
  getters: {
    productos(state, getters, root) {
      return root.productos.productos.length;
    },
    vendidos(state, getters, root) {
      return sumBy(getters.facturasDia(), f => sumBy(f.productos, "cantidad"));
    },
    ventasDia(state, getters, root) {
      return sumBy(getters.facturasDia(), "venta");
    },
    ventasMes(state, getters, root) {
      return sumBy(getters.facturasMes, "venta");
    },
    facturasDia: (state, getters, root) => day => {
      let date = moment(day);
      let facturas = root.factura.facturas;
      return facturas.filter(f => date.isSame(moment(f.id), "day"));
    },
    facturasMes(state, getters, root) {
      let now = moment();
      let facturas = root.factura.facturas;
      return facturas.filter(f => now.isSame(f.id, "month"));
    }
  }
};
