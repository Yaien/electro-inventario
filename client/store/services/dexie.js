import Dexie from "dexie";

const db = new Dexie("code");

db.version(1).stores({
    productos: "++id, nombre",
    usuarios: "++id, correo",
    pedidos: "id",
    facturas: "id"
});

export default db;
