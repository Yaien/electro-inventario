class Session {
    get id() {
        return parseInt(atob(localStorage.getItem("USER_ID")));
    }

    set id(id) {
        localStorage.setItem("USER_ID", btoa(id.toString()));
    }

    clear() {
        localStorage.removeItem("USER_ID");
    }
}

const session = new Session();
module.exports = session;
