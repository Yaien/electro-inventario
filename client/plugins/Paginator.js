class Paginator {
    constructor(show = 10, data = [], page = 1) {
        this.data = data;
        this.show = show;
        this.page = page;
    }

    get items() {
        let start = (this.page - 1) * this.show;
        return this.data.slice(start, start + this.show);
    }

    get pages() {
        return Math.ceil(this.data.length / this.show);
    }

    get rows() {
        return this.data.length;
    }
}

export default {
    Paginator,
    $paginator: (...args) => new Paginator(...args),
    install(Vue) {
        Vue.Paginator = Paginator;
        Vue.prototype.$paginator = this.$paginator;
    }
};
