import moment from "moment";
import "moment/locale/es";

export default {
    install(Vue) {
        Vue.prototype.$moment = moment;
        Vue.filter("date", this.filter);
    },
    filter: (date, format = "LL") => {
        return moment(date).format(format);
    }
};
