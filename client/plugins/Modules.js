function modules() {
    let req = require.context("../modules", true, /\.plugin\.js$/);
    return req.keys().map(key => req(key).default);
}

function components() {
    let req = require.context("../modules", true, /\.vue$/);
    return req.keys().map(key => req(key));
}

export default {
    install(Vue) {
        modules().forEach(mod => Vue.use(mod));
        components().forEach(comp => Vue.component(comp.name, comp));
    }
};
