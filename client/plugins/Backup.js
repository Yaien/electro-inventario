import db from "../store/services/dexie";
import fileSaver from "file-saver";

const collections = [
    {
        table: db.table("productos")
    },
    {
        table: db.table("pedidos"),
        schema: {
            id: id => new Date(id)
        }
    },
    {
        table: db.table("facturas"),
        schema: {
            id: id => new Date(id)
        }
    },
    {
        table: db.table("usuarios")
    }
];

const tables = collections.map(coll => coll.table);

export class Backup {
    /**
     *
     * @param {*} prefix Adicion al nombre del archivo de exportacion
     */
    constructor(prefix = "codebackup") {
        this.prefix = prefix;
    }

    /**
     * Obtiene los datos para guardar en archivo de resplado
     */
    async data() {
        let data = {};
        for (let table of tables) {
            data[table.name] = await table.toArray();
        }
        return JSON.stringify(data);
    }

    /**
     * Obtiene el nombre del archivo a guardar
     */
    get filename() {
        let timestamp = new Date().toLocaleString();
        return `${this.prefix} ${timestamp}.json`;
    }

    /**
     * Descarga el archivo de respaldo
     */
    async export() {
        let data = await this.data();
        let file = new File([data], this.filename, {
            type: "text/json;charset=utf-8"
        });
        fileSaver.saveAs(file);
    }

    /**
     *
     * @param {File} file
     */
    import(file) {
        return db.transaction("rw", ...tables, async () => {
            let data = window.require(file.path);
            for (let { table, schema } of collections) {
                await this.clear(table);
                let items = data[table.name];
                if (schema) {
                    items = items.map(item => this.mapRow(schema, item));
                }
                await table.bulkAdd(items);
            }
        });
    }

    /**
     * Mapea el item para ser insertado en db
     * @param {*} schema
     * @param {*} row
     */
    mapRow(schema, row) {
        let data = {};
        for (let key in schema) {
            let map = schema[key];
            data[key] = map(row[key]);
        }
        return { ...row, ...data };
    }

    /**
     * Elimina toda la informacion de la tabla en db
     * @param {*} table
     */
    async clear(table) {
        let keys = (await table.toArray()).map(row => row.id);
        await table.bulkDelete(keys);
    }
}

export default {
    backup: new Backup(),
    install(Vue) {
        Vue.prototype.$backup = this.backup;
    }
};
