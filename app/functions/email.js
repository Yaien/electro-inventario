const transporter = require("../config/transporter");
const templates = require("../config/templates");
const { randomBytes } = require("crypto");

exports.sendPasswordReset = function(usuario) {
    let contraseña = randomBytes(6).toString("hex");
    transporter.sendMail({
        to: usuario.correo,
        subject: "CodeInventario: Recuperación de contraseña",
        html: templates.passwordReset({ contraseña })
    });
    return contraseña;
};
