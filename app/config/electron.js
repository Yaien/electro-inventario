require("dotenv").config();
const { app, BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");
const development = require("./env/development");

class Electron {
  constructor() {
    this.window = null;
  }

  create() {
    this.window = new BrowserWindow({
      title: "Code Inventario",
      minWidth: 800,
      minHeight: 600,
      width: 1024,
      height: 788,
      autoHideMenuBar: true,
      show: false
    });
    this.window.maximize();
    this.window.on("closed", () => (this.window = null));
    this.window.loadFile(path.join(__dirname, "../../index.html"));
  }

  start() {
    app.on("ready", () => {
      this.create();
      this.config();
    });
    // Quit when all windows are closed.
    app.on("window-all-closed", () => {
      // On macOS it is common for applications and their menu bar
      // to stay active until the user quits explicitly with Cmd + Q
      if (process.platform !== "darwin") {
        app.quit();
      }
    });

    app.on("activate", () => {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (this.window === null) this.createWindow();
    });
  }

  config() {
    if (process.env.NODE_ENV == "development") development();
  }
}

module.exports = Electron;
