const install = require("electron-devtools-installer");

module.exports = async function() {
    try {
        await install.default(install.VUEJS_DEVTOOLS);
    } catch (e) {
        console.error(e);
    }
};
