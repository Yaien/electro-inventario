const pug = require("pug");
const path = require("path");

const options = {
    basedir: path.resolve(__dirname, "../emails"),
    cache: true
};
exports.passwordReset = pug.compileFile(
    path.resolve(options.basedir, "password.pug"),
    options
);
